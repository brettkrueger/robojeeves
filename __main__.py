#!/usr/bin/python
from robojeeves.config import *
from robojeeves.core import *
from robojeeves.custom import *
from robojeeves.bot import *
import sys

if __name__ == "__main__":
	try:
		print("Starting RoboJeeves...")
		bot = twitchBot(config).start() #Begin bot
	except KeyboardInterrupt:
		sys.exit()
