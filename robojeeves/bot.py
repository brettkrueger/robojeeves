import robojeeves.core.irc as irc_
import robojeeves.core.functions as commands
import importlib

class twitchBot():
	def __init__(self,config):
		self.config=config
		self.irc=irc_.irc(config)
		self.socket=self.irc.getSock()

	def start(self): #Start the bot
		irc=self.irc
		sock=self.socket
		config=self.config
		while True: #Loop to read the chat
			data=sock.recv(config["socket_buffer_size"]).decode("UTF-8")
			timeout=False
			if len(data)==0: #If no data is received, reconnect
				print("Connection lost, reconnecting...")
				sock=self.irc.getSock()
			if config["debug"]: print(data) #Prints raw sock data (if debugging enabled)
			irc.checkPing(data) #Check if twitch is pinging you
			if irc.checkForMessage(data):
				message_list=irc.getMessage(data)
				username=message_list["username"]
				message=message_list["message"]
				USERSTATE=message_list["USERSTATE"]
				print("{}: {}".format(username,message))
				if config["log"] and timeout: #Log timeouts (if logging enabled)
					irc.logMessage(username,message.strip("\r")+" <-- Timed Out",True)
				elif config["log"]: #Log everything else (if logging enabled)
					irc.logMessage(username,message,False)
				else: pass

				if message.startswith("!"): #Make sure command is valid
					if commands.isValid(message) or commands.isValid(message.split(' ')[0]):
						command=message
						args=command.split(' ')
						command=command.split(' ')[0]
						del args[0] #Removes command name from args list
						args.append([USERSTATE,username])
						try: #Try to run command
							result=commands.run(command,args)
							if result:
								response="@{} {}".format(username,result)
								irc.sendMessage(response)
								if config["log"]: #Log bot responses to users (if logging enabled)
									irc.logMessage(config["NICK"],response,False)
						except: pass
					else: 
						response="@{} {}".format(username,"Unable to run command. Invalid command or syntax.")
						irc.sendMessage(response)
						if config["log"]: #Log failures because you're a failure (if logging enabled)
							irc.logMessage(config["NICK"],response,True)
				else: pass
