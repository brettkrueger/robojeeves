import os,requests,json,re,sys,random,inspect
import robojeeves.core.irc as irc_
from robojeeves.config import *
dir=os.path.realpath("robojeeves/core/")
raffle_status=False
raffle_users=[]

class Commands():
	def __init__(self,config):
		self.config=config
		self.irc=irc_.irc(config)

	def list(self):
		cmds=[]
		for i in inspect.getmembers(self,inspect.ismethod):
			cmds.append(i[0])
		cmds.remove("__init__")
		return "Available commands: {}".format(str(cmds))

	def hi(self):
		return "Hello!"
	
	def source(self):
		return "My source? So nice of you to ask: https://github.com/brettkrueger/robojeeves/"

	def soon(self):
		return "Coming soon to a RoboJeeves near you: Custom commands functionality, Remote ban, Remote kill for broadcaster, Add/Remove/Modify commands, Subscriber Levels"

	def ban(self,username,reason,duration,args):
		USERSTATE=args[len(args)-2]
		if re.search('@badges=\w*,?(broadcaster|moderator),?\w*/[0-9]+',self.irc.userState(USERSTATE)[0]):
			return "Unable to ban {} at this time, banning not supported. Please ban user manually.".format(username)
#			return "Banning {}".format(username)
#			self.sendMessage(".ban {} {} - {}".format(username,str(duration),reason))
		else: return "Only broadcaster/moderators may ban users."

	def quit(self,args): #Allows broadcaster to stop bot remotely
		USERSTATE=args[len(args)-2]
		if re.search("@badges=w*,?broadcaster,?\w*/[0-9]+",self.irc.userState(USERSTATE)[0]):
			return "Unable to quit at this time. If you are the owner, please kill me manually." #"Quit command received. Exiting..."
		else: return "Only the broadcaster may kill me."

	def add(self,args): #Usage - !add <command_name> "text"
		irc=self.irc
		return "Adding not yet supported."

	def remove(self,args): #Usage !remove or !rm <command_name>
		irc=self.irc
		return "Removing not yet supported."	

	def rm(self,args):
		return self.remove(args)

	def modify(self,args): #Usage !modify <command_name> "text"
		irc=self.irc
		return "Modification not yet supported."

	def twitter(self):
		if self.config["twitter"]:
			return "Check out our Twitter: https://twitter.com/{}.".format(self.config["twitter"])
		else: exit(1)

	def t(self):
		return self.twitter()

	def youtube(self):
		if self.config["youtube"]:
			return "Check out our YouTube channel: https://youtube.com/{}.".format(self.config["youtube"])
		else: exit(1)

	def yt(self):
		return self.youtube()

	def friendcode(self):
		if self.config["friend_code"]:
			return "Friend code: {}".format(self.config["friend_code"])
		else: exit(1)

	def fc(self):
		return self.friendcode()

	def steam(self):
		if self.config["steam"]:
			return "Steam username: {}".format(self.config["steam"])
		else: exit(1)
