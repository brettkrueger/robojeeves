import socket,os,re,time,sys,json,requests
from robojeeves.config import *

class irc():
	def __init__(self,config):
		self.config=config

	def isCommand(self,message,validCommands):
		for command in validCommands:
			if command==message:
				return True

	def userList(self):
		return requests.get('https://tmi.twitch.tv/group/user/' + self.config['CHAN'].replace('#', '') + '/chatters').json()

	def userState(self,USERSTATE):
		return (USERSTATE.split(';'))

	def checkForMessage(self,data):
		if re.search(':\w+!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #\w+ :', data):
			return True

	def checkForLink(self,message): #Check for links in messages
		if re.search('https?:\/\/\w*\.?\w+\.\w+\/?', message):
			return True

	def checkPing(self,data): #Check if you are being pinged
		if data[:4] == "PING":
			self.sock.send("PONG tmi.twitch.tv\r\n".encode())

	def getMessage(self,data): #Return username & message from data
		return {
			"username": re.findall(':\w+', data)[0].replace(':', ''),
			"message": re.findall('PRIVMSG #[a-zA-Z0-9_]+ :(.+)', data)[0].strip('\r'),
			'USERSTATE':data.split()[0],
		}

	def sendMessage(self,data):
		self.sock.send("PRIVMSG {} :{}\n".format(self.config["CHAN"],data).encode())

	def logMessage(self,username,message,timeoutStatus): #Logs chat messages to file
		if self.config["log_dir"] == '':
			dir=os.path.realpath("robojeeves/logs/")
		else: dir=os.path.realpath(self.config["log_dir"])
		with open(dir + "/" + time.strftime('%m-%d-%Y') + '.log', 'a+') as file:
			file.write('{} - {}: {}\n'.format(time.strftime('%H:%M:%S'),username,message))


	def timeout(self,username,message,duration): #Timeout user with custom error message
		self.sendMessage(".timeout {} {}".format(username,str(duration)))
		self.sendMessage("@ {} <----- {}".format(username,message))

	def getSock(self): #Create socket object, login, and join channel configured in config.py		
		sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		self.sock=sock
		try: sock.connect((self.config["HOST"],self.config["PORT"])) #Attempts to connect to host/port
		except: print("Cannot connect to server ({}:{}).".format(self.config["HOST"],self.config["PORT"]))
		#Send login and channel information to server
		sock.send('USER {}\r\n'.format(self.config['NICK']).encode())
		sock.send('PASS {}\r\n'.format(self.config['PASS']).encode())
		sock.send('NICK {}\r\n'.format(self.config['NICK']).encode())
		sock.send('CAP REQ :twitch.tv/membership\r\n'.encode())
		sock.send('CAP REQ :twitch.tv/tags\r\n'.encode())
		sock.send('CAP REQ :twitch.tv/commands\r\n'.encode())
		sock.send('JOIN {}\r\n'.format(self.config['CHAN']).encode())
		return sock
