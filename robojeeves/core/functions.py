from robojeeves.config import *
import robojeeves.core.commands as commands_
import robojeeves.custom.commands as custom
import inspect,time,importlib

commands=commands_.Commands(config)

def isValid(command):
	command=command.replace("!","").rstrip().lower()
	try: #Attempt to set function variable to provided command
		function=getattr(commands,command)
		return True
	except AttributeError: 
		importlib.reload(custom)
		try:
			function=getattr(custom,command)
			return True
		except AttributeError: return False
	except: return False

def run(command,args):
	command = command.replace('!', '').rstrip().lower()
	try: function=getattr(commands,command)
	except:
		try: function=getattr(custom,command)
		except AttributeError: print("Command '{}' not found".format(command))
	try:
		inspect.getargspec(function)[0][1]
		return function(args)
	except IndexError: return function()
