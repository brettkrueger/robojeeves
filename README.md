# RoboJeeves

Twitch/IRC Bot

To install:

```
git clone https://github.com/brettkrueger/robojeeves.git
```
To run:

```
export PATH=$PATH:/path/to/robojeeves/
robojeeves.sh
```
OR
```
python /path/to/robojeeves/
```
